#!/usr/bin/env bash

function run() {
  if [ "$REMOTE_MODE" = true ]; then
    ssh ${CLUSTER_USER}@${CLUSTER_HOST} $1
  else
    eval $1
  fi
}

function create_module_file {
    envfile_path=$1
    env_name=`echo $envfile_path | awk -F/ '{print $(NF-1)}'`
    env_filename=`echo $envfile_path | awk -F/ '{print $(NF)}'`
    env_version="${env_filename%.*}"
    env_path=${CONDA_HOME}/envs/${env_name}-${env_version}

    run "mkdir -p ${MODULE_FILE_PATH}/${env_name}"

    if [ "$REMOTE_MODE" = true ]; then
      module_file="/tmp/modulefile"
    else
      module_file="${MODULE_FILE_PATH}/${env_name}/${env_version}"
    fi

    cp ci/modulefile $module_file
    echo "conflict ${env_name}" >> $module_file
    if run "[ -d ${env_path}/bin ]"; then
        echo "prepend-path PATH ${env_path}/bin" >> $module_file
    fi
    if run "[ -d ${env_path}/include ]"; then
        echo "prepend-path CPATH ${env_path}/include" >> $module_file
    fi
    if run "[ -d ${env_path}/lib ]"; then
        echo "prepend-path LD_LIBRARY_PATH ${env_path}/lib" >> $module_file
    fi
    if run "[ -d ${env_path}/man ]"; then
        echo "prepend-path PATH ${env_path}/man" >> $module_file
    fi
    if [ "$REMOTE_MODE" = true ]; then
      scp $module_file ${CLUSTER_USER}@${CLUSTER_HOST}:${MODULE_FILE_PATH}/${env_name}/${env_version}
      rm /tmp/modulefile
    fi

}

CONDA_BIN=$CONDA_HOME/bin/conda
run "${CONDA_BIN} --version"

#because gitlab-ci will only keep the artifacts 5 minutes
change_env_list="change_env_backup.list"
cp $1 $change_env_list

exit_code=0

# creating envs
for envfile in */*/*.y*ml; do
    if grep -Fxq "$envfile" $change_env_list
    then
      if [ "$REMOTE_MODE" = true ]; then
        env_file_path="/tmp/${CI_JOB_ID}-envfile"
        scp ${envfile} ${CLUSTER_USER}@${CLUSTER_HOST}:$env_file_path
      else
        env_file_path=$envfile
      fi

      echo "$envfile: Trying to create environment..."

      run "${CONDA_BIN} env create -f ${env_file_path}" 2> stderr
      if [ $? -eq 0 ]; then
          echo "$envfile: Creating modulefile..."
          create_module_file $envfile
          echo "$envfile: created"
      else
          if grep -q "CondaValueError: prefix already exists:" stderr; then
              echo "$envfile: Environment already exists"
              if grep -q "^${envfile}$" $change_env_list; then
                  echo "$envfile: Updating environment..."
                  run "${CONDA_BIN} env update -f ${env_file_path}" 2> stderr
                  if [ $? -eq 0 ]; then
                      echo "$envfile: Updating modulefile..."
                      create_module_file $envfile
                      echo "$envfile: updated"
                  else
                      echo -n "$envfile: error while updating"
                      cat stderr
                      exit_code=1
                  fi
              fi
          else
              echo -n "$envfile: error while creating"
              cat stderr
              exit_code=1
          fi
      fi

      if [ "$REMOTE_MODE" = true ]; then
        run "rm $env_file_path"
      fi
    fi
done

exit $exit_code
